$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
      console.log('el modal se está mostrando');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled',true);
      $('.boton-ver-mas').removeClass('btn-primary');
      $('.boton-ver-mas').addClass('btn-outline-primary');
      $('.boton-ver-mas').prop('disabled',true);
      
    });
    $('#contacto').on('shown.bs.modal', function (e) {
      console.log('el modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
      console.log('el modal se está ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
      console.log('el modal se ocultó');
      $('#contactoBtn').prop('disabled',false);
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
      $('.boton-ver-mas').prop('disabled',false);
      $('.boton-ver-mas').removeClass('btn-outline-primary');
      $('.boton-ver-mas').addClass('btn-primary');
      
    });
});